# This is when running the script from the root mgris directory
import sys, os

s = os.path.dirname(os.path.abspath(__file__))
s = s[: s.index("Contexts/")]
sys.path.insert(0, s)

import argparse
import numpy as np
import pandas as pd
from Library.lib_mc import logsum_simple
from Library.lib_main import readparam, read_data, check_duplicates
from Library.lib_misc import FileExists, inputRL


def main(args=None):
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"
    grid_directory = root_directory + "Grids/"

    # =================================================================
    # get grids from context input file
    with open(root_directory + "context_input.txt", "r") as f:
        input_file_lines = f.readlines()
    # remove comments and empty lines, and carriage returns
    input_file_lines = list(
        np.array(
            [
                l.replace("\n", "").strip()
                for l in input_file_lines
                if l[0] != "#" and l != "\n"
            ]
        )
    )

    grid_file = readparam(input_file_lines, ["grid_file"])
    labels_file = [
        grid_directory + "list_observables.dat",
    ]  # convenient list of keys
    params_file = [
        grid_directory + "list_parameters.dat",
    ]  # convenient list of keys
    files = [
        grid_directory + grid_file,
    ]

    post_processing_file = readparam(input_file_lines, ["post_processing_file"])
    if post_processing_file is not None:
        post_processing_file = post_processing_file[0]
        labels_file += [grid_directory + "list_post-processing.dat"]
        files += [
            grid_directory + post_processing_file,
        ]

    # =================================================================
    # pre-process and convert

    for i, f in enumerate(files):
        print("\n------------------------------------")

        # keeping all columns...
        columns = None
        # ...or keeping only some columns for memory issues
        # columns = readparam(input_file_lines, ['primary_parameters'])+['Stop_Av10'] #mandatory
        # columns += ['Al22660.35A',] #some tracers
        data = read_data(
            f,  # will read either .csv or .fth files
            columns=columns,
            delimiter=readparam(input_file_lines, ["delimiter"]),  # if ascii
        )

        data = data.astype("float32")

        # -------------------------------------------------------------------
        print("Making some changes...")
        # UPDATE

        # primary/secondary parameters
        data["Zsun"] = data["Z"] + (12 - 8.69)
        data["Zsun"] = np.round(data["Zsun"], 3)  # keep precision from table
        del data["Z"]
        data["age"] = np.round(data["age"], 3)  # keep precision from table

        # # round hbfrac to get a ~complete grid (--> will generate some duplicates...)

        # rounds = np.arange(0.05, 1.05, 0.05)
        # rounds = np.round(sorted(set(data["hbfrac"])), 2)
        # trying to maximize completion of grid while minimizing duplicates
        # rounds = np.array(
        #     [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
        # )
        # x = np.subtract.outer(np.array(data["hbfrac"]), rounds)
        # y = np.argmin(abs(x), axis=1)
        # data["hbfrac"] = rounds[y]
        data["hbfrac"] = np.round(data["hbfrac"], 1)
        data["hbfrac"] = np.log10(data["hbfrac"])

        # force N/O vs O/H?
        for zz in list(set(data["Zsun"])):
            no = np.log10(10 ** -1.732 + 10 ** (zz + 8.7 - 12 + 2.19))
            rounds = list(set(data["NO"]))
            noref = rounds[np.argmin(abs(rounds - no))]
            for no in rounds:
                if no != noref:
                    data.drop(
                        data.loc[data["Zsun"] == zz].loc[data["NO"] == no].index,
                        inplace=True,
                    )
        del data["NO"]

        # check duplicates after primary parameters have been renamed
        data = check_duplicates(
            data, columns=readparam(input_file_lines, ["primary_parameters"])
        )

        # rename/clean column labels
        # data = data.rename(columns={tmp: tmp.replace(' ', '').replace("'", '').replace('(', '').replace(')', '') for tmp in data.keys()})

        # observables and primary parameters
        if i == 0:  # ony for main grid
            print("- Primary grid")

            # # before we round hbfrac, let's calculate the total Hbeta flux and escaping Hbeta-radiation
            # preferring to get Hbeta tot from hbfrac=1 lines because value from obserbed Hbeta/hbfrac is not exactly the same
            from copy import deepcopy

            # # from hbfrac vs. U correlation scanning OI/OIII
            # expectedhbfrac = (
            #     -0.0764 * data["U"] ** 4
            #     - 0.928 * data["U"] ** 3
            #     - 3.992 * data["U"] ** 2
            #     - 7.30 * data["U"] ** 1
            #     - 5.183
            # )
            # data["corroffset"] = np.round(
            #     np.log10(data["hbfrac"]) - expectedhbfrac, 1
            # )  # --> also needed in pp table

            tmp = deepcopy(data)
            params = readparam(input_file_lines, ["primary_parameters"])

            # reference to get Q0 and Q(Hbeta): radiation-bounded
            tmp = tmp[params]
            tmp["hbfrac"] = 0.0
            ref = pd.merge(
                tmp.astype(np.float32), data.astype(np.float32), on=params, how="left",
            )

            # number of ionizing photons proportional to Hbeta for radiation-bounded model (fesc = 0, hbfrac == 1, log hbfrac = 0)
            data["Q0"] = ref["H__1_486133A"]
            # number of ionizing photons *absorbed*
            data["Q(Hbeta)"] = data["H__1_486133A"]
            data["fescQ"] = np.log10(
                1 - 10 ** (data["H__1_486133A"] - ref["H__1_486133A"])
            )

            data.loc[~np.isfinite(data["fescQ"]), "fescQ"] = -10
            # data.loc[np.isnan(data["Q(Hbeta)"])]["Q(Hbeta)"] = 10
            # data = data.interpolate(method="linear")  # try to interpolate nans...?

            # tests
            data["Ha_pp"] = data["H__1_656281A"]

            # duplicates edges (temporary test)
            # tmp = data.loc[data["hbfrac"] == -1]
            # tmp["hbfrac"] = -2
            # data = pd.concat([data, tmp], axis=0)
            # tmp = data.loc[data["hbfrac"] == 0]
            # tmp["hbfrac"] = 1
            # data = pd.concat([data, tmp], axis=0)

            # normalize so that Q0 is always the same? i.e., forcing Hbeta to trace Q(H) thus fesc when combining several models
            # tmp = deepcopy(data["Q0"])
            # for o in observable_list:
            #     data[o] -= tmp - np.median(tmp)
            # breakpoint()

            # normalize to avoid large numbers?
            # for o in observable_list:
            #    data[o] /= 1e50

            # adding some sums
            data["S26716+30A"] = logsum_simple(
                [data["S__2_671644A"], data["S__2_673082A"]]
            )
            data["BLND_124000A"] = logsum_simple(
                [data["N__5_123882A"], data["N__5_124280A"]]
            )
            data["BLND_280000A"] = logsum_simple(
                [data["MG_2_279553A"], data["MG_2_280271A"]]
            )
            data["BLND_388900A"] = logsum_simple(
                [data["HE_1_388863A"], data["H__1_388905A"]]
            )

            # observables not in the main table will be also looked for in the post-processing table
            # but you can always include here some parameters from the post-processing table, e.g., to use as observable IF rows are exactly in the same order in both the main table and the post-processing table
            # data['MHI'] = pd.read_csv(files[1], sep=readparam(input_file_lines, ['delimiter']))['MHI']

            # final duplicate check: if primary parameters have been modified, check that we have only single sets
            data = check_duplicates(
                data, columns=readparam(input_file_lines, ["primary_parameters"])
            )

            ffile = grid_directory + "/model_grid.fth"

            with open(params_file[i], "w") as of:
                for p in readparam(input_file_lines, ["primary_parameters"]):
                    of.write("\n\nPrimary parameter: {}\n".format(p))
                    of.write(str(sorted(set(data[p].values))))
            print("Writing list of parameters in {}".format(params_file[i]))

        if i == 1:  # only for post-processing grid
            print("- Secondary grid:")
            #    data['...'] = data['...']  - ...

            # # from hbfrac vs. U correlation scanning OI/OIII
            # expectedhbfrac = (
            #     -0.0764 * data["U"] ** 4
            #     - 0.928 * data["U"] ** 3
            #     - 3.992 * data["U"] ** 2
            #     - 7.30 * data["U"] ** 1
            #     - 5.183
            # )
            # data["corroffset"] = np.round(
            #     np.log10(data["hbfrac"]) - expectedhbfrac, 1

            # fesc was already calculated precisely beforehand
            # # round hbfrac to get a ~complete grid (--> will generate some duplicates...)

            # replace primary parameters
            # not too important since merging of tables is performed on common column = model number
            tmp = read_data(
                grid_directory
                + "/model_grid.fth",  # will read either .csv or .fth files
                columns=columns,
                delimiter=readparam(input_file_lines, ["delimiter"]),  # if ascii
            )
            data["hbfrac"] = tmp["hbfrac"]
            data["fescQ"] = tmp["fescQ"]
            data.loc[~np.isfinite(data["fesc(Hb)"]), "fesc(Hb)"] = -10

            ffile = grid_directory + "/model_grid_post_processing.fth"

        # -------------------------------------------------------------------
        # keep a convenient (full) list of observables
        observable_list = [
            d
            for d in data.keys()
            if d not in readparam(input_file_lines, ["primary_parameters"])
        ]

        with open(labels_file[i], "w") as of:
            for o in observable_list:
                of.write(o + "\n")
        print("Writing list of observables in {}".format(labels_file[i]))

        # -------------------------------------------------------------------
        # change to luminosity?
        # [np.log10(np.power(10,values[list_of_lines[j]][ind[0]])/(4*np.pi*(distance*1e6*pc2cm)**2)) for j in range(n_lines)]

        # this is to avoid appending
        # ffile = os.path.splitext(f)[0]+'.fth'
        if os.path.exists(ffile):
            os.remove(ffile)

        print("Writing new Feather file... {}".format(ffile))
        data.to_feather(ffile)

    print("------------------------------------")


# --------------------------------------------------------------------
if __name__ == "__main__":
    main()
