import importlib

try:
    import Library.lib_input
except:
    print("Make sure to setup the pythonpath environment variable, see INSTALL.")
    exit()

importlib.reload(Library.lib_input)
from Library.lib_input import *

# Retrieve list of known input parameters

params = get_params()

# ------------------------------------------------
# output directory (absolute or relative from the folder the mgris code is launched)

params["output"].value = "Results_example_BOND/"

# ------------------------------------------------
# Setting a context

params["context"].value = "Contexts/mgris_bond"

# ------------------------------------------------
# Setting a configuration

# Setting a pre-defined configuration?
params["USE configuration"].value = "1C1S_shell"

# Setting some hard constraints
params["select"].value = [
    "hbfrac [1,1]",  # i.e., force hbfrac=1
]  # possible to have several range commands
params["select"].comment()

# Setting some specific priors to complement/replace pre-defined configuration
# all distributions are assumed to be single (i.e., not described as power-law, normal etc...) and with nearest neighbor interpolation
params[
    "BEGIN configuration"
].value = """n_comps 1
age (0) < 1
distrib Z single linear
"""
params["BEGIN configuration"].comment()

# ------------------------------------------------
# Systematic uncertainties

params["sysunc"].value = [
    "elemental_abundances_O 0.2 ['BLND_436300A', 'O__3_500684A']",
]
params["sysunc"].comment()

# ------------------------------------------------
# List of observables

labels = [
    "BLND_372700A",
    "H__1_486133A",
    "NE_3_386876A",
    "BLND_436300A",
    "O__3_500684A",
    "HE_1_587564A",
    "N__2_658345A",
]
values = [2.8600e-01, 1, 1.5800e-01, 6.4000e-02, 1.9100e00, 6.4000e-02, 9.0000e-03]
errors = [6.7779e-03, 1e-6, 4.7319e-03, 3.1699e-03, 4.4538e-02, 3.1699e-03, 2.0052e-03]

params["BEGIN observations"].value = ""
for i, l in enumerate(labels):
    params["BEGIN observations"].value += "{} {} {}\n".format(l, values[i], errors[i])
params["BEGIN observations"].extras = {
    "scale": "linear",
    "scale_factor": 1,
    "delta_add": 0.1,
}
# ------------------------------------------------
# Use observation sets instead (i.e., several groups of observations)?

# params['observation_sets'] = [InputParameter('BEGIN observations'),
#                               InputParameter('BEGIN observations'), ]

# params['observation_sets'][0].value = ''
# for i,l in enumerate(labels):
#     params['observation_sets'][0].value += '{} {}\n'.format(l, values[i])
# params['observation_sets'][0].extras = {'delta_ref': 0.1, 'scale': 'log', 'scale_factor': 10, 'name': 'setobs1'}

# params['observation_sets'][1].value = ''
# for i,l in enumerate(labels):
#     params['observation_sets'][1].value += '{} {}\n'.format(l, values[i])
# params['observation_sets'][1].extras = {'delta_ref': 0.1, 'scale': 'log', 'scale_factor': 0, 'name': 'setobs2'}

# ------------------------------------------------
# Which observable do we use as constraint for the prior on the scaling parameter?

params["use_scaling"].value = "'all'"
# params['use_scaling'].value = "'H__1_486133A'"

# ------------------------------------------------
# Predict some unobserved quantities?

params["obs_to_predict"].value = [
    "HE_1_447149A",
]

# Predict some secondary parameters?
params["secondary_parameters"].value = ["fesc(Ha)"]
params["secondary_parameters"].comment()

# ------------------------------------------------
# Activate some context-specific rules

params["active_rules"].value = ["extinction"]
params["active_rules"].comment()

# ------------------------------------------------
# Global parameters can be overridden

params["nsamples_per_chain_max"].value = 100000
params["nsamples_per_chain_max"].comment()

# ---------------------------------------
# Write the resulting input file

make_input(params, "input_example_BOND.txt")
